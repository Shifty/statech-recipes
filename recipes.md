# Recipes

- [Advanced Pump](#advanced-pump)
- [Large Advanced Pump](#large-advanced-pump)
- [Analog Circuit](#analog-circuit)

## Analog Circuit
| Ingredient | Quantity |
| ---------- | -------- |
├ Analog Circuit Board | 1
├─ Rubber Sheet | 2
├─ Copper Plate | 1
└── Copper Ingot | 1
||
├ Capacitor | 2
├─ Gold Plate | 2
├── Gold Ingot | 1
├─ Copper Wire | 2
├── Copper Plate | 1
└─── Copper Ingot | 1
||
├ Inductor | 1
├─ Copper Wire | 8
├── Copper Plate | 1
├─── Copper Ingot | 1
├─ Steel Rod | 1
└── Steel ingot | 1
||
├ Resistor | 2
├─ Copper Fine Wire | 2
├── Copper Wire | 1
├─── Copper Plate | 1
├──── Copper Ingot | 1
├─ Coal Dust | 1
└── Coal | 1

## Advanced Motor
| Ingredient | Quantity |
| ---------- | -------- |
├ Annealed Copper Wire x 4
├─ Annealed Copper Plate | 1
└── Annealed Copper Ingot
||
├ Magnetic Stainless Steel Rod | 1
├─ Stainless Steel Rod | 1
├── Stainless Steel Ingot | 1
└─ Neodymium | 1
||
├ Stainless Steel Rod | 2
├─ Stainless Steel Ingot | 1
├ Aluminum Cable | 2
├─ Aluminum Wire | 3
├── Aluminum Plate | 1
└─── Aluminum Ingot | 1

## Advanced Pump
| Ingredient | Quantity |
| ---------- | -------- |
└ [Advanced Motor](#advanced-motor) | 1
||
├ Copper Rotor | 2
├─ Copper Blade | 4
├── Copper Curved Plate | 2
├─── Copper Plate | 1
├── Copper Rod | 1
├─── Copper Ingot | 1
└─ Copper Ring | 1
||
├ Fluid Pipe | 3
├─ Bronze Curved Plate | 6
├── Bronze Curved Plate | 6
├─── Bronze Plate | 1
└──── Bronze Ingot | 1
||
├ Glass Pane x 1/16
└─ Glass | 6
||
├ Stainless Steel Rotor | 3
├─ Stainless Steel Blade | 4
├─── Stainless Steel Curved Plate | 2
├──── Stainless Steel Plate | 1
├─── Stainless Steel Rod | 1
├──── Stainless Steel Ingot | 1
└─ Stainless Steel Ring | 1

## Large Advanced Motor
| Ingredient | Quantity |
| ---------- | -------- |
└ [Advanced Motor](#advanced-motor) | 6
||
└ [Processing Unit](#processing-unit) | 1
||
├ Titanium Rod | 2
└─ Titanium Ingot | 1

## Large Advanced Pump
| Ingredient | Quantity |
| ---------- | -------- |
├ [Advanced Pump](#advanced-pump) | 6
||
├ Titanium Rotor | 3
├─ Titanium Blade | 4
├─ Titanium Ring | 1
└ WIP